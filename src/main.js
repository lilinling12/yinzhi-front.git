// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import cookie from 'js-cookie'


Vue.use(ElementUI);

Vue.config.productionTip = false
const whiteList = ['/home', '/login', '/register', '/makerList', '/demand', '/refreshmaker', '/makersDetails'] // 不需要登陆的页面
router.beforeEach(function (to, from, next){
  let token = cookie.get('yinzhi_jwt_token')
  if (!token) { // 没登录
    if (whiteList.indexOf(to.path) !== -1) { // 白名单
      next()
    } else {
        next('/login')
    }

  } else {
    if (to.path === '/login') { //  跳转到
      next({path: '/'})
    }
    next()
    }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
