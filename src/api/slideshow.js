import request from '@/utils/request'

export default{
  getSlideshow(){
    return request({
      url:'/model/panelcontent/list',
      method: 'get',
    })
  }
}
