import request from '@/utils/request'

export default{
  newDemand(){
    return request({
      url: '/demand/demand/newdemand/list',
      method: 'get',
    })
  },

  demandList(params){
    return request({
      url:'/demand/demand/list',
      method: 'get',
      params: params
    })
  },

  infoDemand(demand){
    return request({
      url:'/demand/demand/auth/add',
      method: 'post',
      data: demand
    })
  },

  getUserDemand(){
    return request({
      url: 'demand/demand/auth/userdemand',
      method: 'get'
    })
  }
}
