import request from '@/utils/request'

export default{
  submitOrder(makerId){
    return request({
      url: `/order/orderweb/auth/submitOrder/${makerId}`,
      method: 'post',
    })
  },

  getUserOrderList(params){
    return request({
      url: '/order/userOrderWeb/auth/userOrderList',
      method: 'post'
    })
  }
}
