import request from '@/utils/request'
// import cookie from 'js-cookie'


export default{
  submitLogin(userLogin) {
    return request({
      url: '/user/user/login',
      method: 'post',
      data: userLogin
    })
  },

  getLoginInfo () {
    return request({
      url: '/user/user/get-login-info',
      method: 'get',
      // headers: { 'token': cookie.get('yinzhi_jwt_token') }
    })
  }
}
