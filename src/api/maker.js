import request from '@/utils/request'

export default{
  hotMakersList(){
    return request({
      url: '/user/makers/hotmaker/list',
      method: 'get',
    })
  },

  makerList(params){
    return request({
      url:'/user/makers/list',
      method: 'get',
      params: params
    })
  },

  makerInfo(maker){
    return request({
      url: '/user/makers/auth/savemakerinfo',
      method: 'post',
      data: maker
    })
  },

  getMakerById(makerId) {
    return request({
      url: `/user/makers/addmaker/${makerId}`,
      method: 'get'
    })
  }
}
