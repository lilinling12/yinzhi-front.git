import request from '@/utils/request'

export default{
  policy(dataObj){
    return request({
        url:'/thirdparty/oss/policy',
        method: 'get',
        data: dataObj
      })
  }
}
