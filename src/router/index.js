import Vue from 'vue'
import Router from 'vue-router'
const Index = () => import('@/pages/index.vue')
const Login = () => import('@/pages/login/login.vue')
const Register = () => import('@/pages/login/register.vue')
const Home = () => import('@/pages/home/home.vue')
const Demand = () => import('@/pages/demand/demand.vue')
const MakerList = () => import('@/pages/maker/makerList.vue')
const MakersDetails = () => import('@/pages/maker/makersDetails.vue')
const MakerInfo = () => import('@/pages/maker/makerInfo.vue')
const RefreshMaker = () => import('@/pages/refresh/refreshmaker.vue')
const User = () => import('@/pages/user/user.vue')
const Information = () => import('@/pages/user/information/information.vue')
const UserInformation = () => import('@/pages/user/information/userInformation.vue')
const CheckOut = () => import('@/pages/checkOut/checkOut.vue')
const OrderList = () => import('@/pages/user/order/orderList.vue')
const Order = () => import('@/pages/order/order.vue')
const Payment = () => import('@/pages/order/payment.vue')

Vue.use(Router)

const VueRouterPush = Router.prototype.push
Router.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch(err => err)
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index,
      redirect: '/home',
      children: [
        { path: 'home', component: Home },
        { path: 'demand', component: Demand },
        { path: 'makerList', component: MakerList },
        { path: '/refreshmaker', name: 'refreshmaker', component: RefreshMaker }
      ]
    },

    { path: '/login', name: 'login', component: Login },
    { path: '/register', name: 'register', component: Register },
    { path: '/makersDetails', name: 'makersDetails', component: MakersDetails },
    { path: '/makerInfo', name: 'makerInfo', component: MakerInfo },

    {
      path: '/user',
      name: 'user',
      component: User,
      redirect: '/user/information',
      children: [
        {
          path: 'information', name: '账户资料', component: Information, redirect: 'information/userInformation',
          children: [
            {path: 'userInformation', name: '用户资料', component: UserInformation}
          ]
        },
        { path: 'orderList', name: '订单列表', component: OrderList}
      ]
    },

    {path: '/checkOut', name: 'checkOut', component: CheckOut},

    {
      path: '/order',
      name: 'order',
      component: Order,
      children: [
        {path: 'payment', name: 'payment', component: Payment},
      ]
    },
  ]
})
